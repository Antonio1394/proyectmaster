@extends('general.layouts.principal')
@section('title', 'User')
@section('dashboardMenu', 'active')

@section('styles')
    {!! Html::style('assets/own/dist/sweetalert.css') !!}
    {!! Html::script('assets/own/js/socket.io.js') !!}
@endsection

@section('content')
    <div class="main-body" v-show="!chatview">
        
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Alertas</h4>
            </div>
        </div>

        {{-- <div class="row" id="alerts-news">
            <div class="col-lg-12">
                <section id="cd-timeline" class="cd-container">
                    <div class="cd-timeline-block" v-for="alert in alerts">
                        <div v-if="alert.code_type == 1" class="cd-timeline-img cd-danger">
                            <i class="fa fa-users"></i>
                        </div>

                        <div v-if="alert.code_type == 2" class="cd-timeline-img cd-warning">
                            <i class="fa fa-car"></i>
                        </div>

                        <div v-if="alert.code_type == 3" class="cd-timeline-img cd-warning">
                            <i class="fa fa-diamond"></i>
                        </div>

                        <div v-if="alert.code_type == 4" class="cd-timeline-img cd-warning">
                            <i class="fa fa-globe"></i>
                        </div>

                        <div v-if="alert.code_type == 5" class="cd-timeline-img cd-danger">
                            <i class="fa fa-ambulance"></i>
                        </div>

                        <div class="cd-timeline-content">
                            <h3>@{{ alert.alert_type }}</h3>
                            <p v-if="alert.description"><strong>Descripción:</strong> @{{ alert.description }}.</p>
                            {{-- vehiculos 
                            <p v-if="alert.type"><strong>Tipo:</strong> @{{ alert.type }}.</p>
                            <p v-if="alert.brand"><strong>Marca:</strong> @{{ alert.brand }}.</p>
                            <p v-if="alert.color"><strong>Color:</strong> @{{ alert.color }}.</p>
                            <p v-if="alert.plate"><strong>Placa:</strong> @{{ alert.plate }}.</p>
                            {{-- Alerta Heridos/Fallecidos -
                            <p v-if="alert.quantity"><strong>Cantidad:</strong> @{{ alert.quantity }}.</p>
                            <p v-if="alert.personType"><strong>Tipo de persona:</strong> @{{ alert.personType }}.</p>
                            <p v-if="alert.alertType"><strong>Tipo de alerta:</strong> @{{ alert.alertType }}.</p>
                            <p v-if="alert.cause"><strong>Causa:</strong> @{{ alert.cause }}.</p>
                            {{-- Robo general -
                            <p v-if="alert.robbery"><strong>Robo a:</strong> @{{ alert.robbery }}.</p>
                            <p v-if="alert.individual"><strong>Descripción del delincuente:</strong> @{{ alert.individual }}.</p>
                            <p v-if="alert.object"><strong>Objeto robado:</strong> @{{ alert.object }}.</p>
                            {{-- Robo general --
                            <p v-if="alert.person"><strong>Propietario:</strong> @{{ alert.person }}.</p>
                            {{-- vehiculos --
                            <p v-if="alert.address"><strong>Dirección:</strong> @{{ alert.address }}.</p>
                            <span class="cd-date">@{{ alert.date }}</span>
                        </div>
                    </div>
                </section>
            </div>
        </div> --}}
    </div>

   
@endsection


@section('scripts')
    @include('templates.administrator.components.footer')

    {!! Html::script('assets/own/dist/sweetalert.min.js') !!}
    {!! Html::script('assets/plugins/moment/moment.js') !!}
    {!! Html::script('assets/pages/jquery.chat.user.js') !!}
    {!! Html::script('assets/own/js/chat/module_agent.js') !!}
    {!! Html::script('assets/own/js/chat/agent.js') !!}
    {!! Html::script('assets/own/js/vue.js') !!}
    {!! Html::script('assets/own/js/alertsRealTime.js') !!}
@endsection