@extends('general.layouts.principal')
@section('title', 'Administrador')
@section('userMenu', 'active')

@section('styles')
  {!! Html::style('assets/own/dist/sweetalert.css') !!}
  {!! Html::script('assets/own/js/socket.io.js') !!}
@endsection

@section('content')
  <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
  <input type="hidden" name="id" id="idUser" value='{{Auth()->user()->id}}'>
  <div class="main-body" v-show="!chatview">
    <div class="show-chat">
      <button class="btn btn-success" @click="showchat">
          <i class="fa fa-commenting-o" aria-hidden="true"></i>
      </button>
      <button v-if="varCheckAlert" class="btn btn-danger" @click="checkAlert">
          <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
      </button>
    </div>
    <div class="row">
        <div class="col-lg-12">
    			<div class="panel panel-border panel-custom">
    				<div class="panel-heading">
    					<h3 class="panel-title">Ubicacion de Agente</h3>
    				</div>
    				<div class="panel-body">
                <div class="row text-center">
                  <div class="col-lg-5">
                    <div class="card-box">
                      <h4 class="text-dark header-title m-t-0 m-b-30">Informacion sobre la Ubicacion</h4>
                      <div class="widget-chart text-center">
                        <div class="display:inline;width:150px;height:150px;">
                        </div>
                        <button type="button" class="btn btn-block btn-lg btn-primary waves-effect waves-light" name="button" onClick="location.reload();" >Actualizar Ubicacion</button>
                        <ul class="list-inline m-t-15">
                          <li>
                            <h5 class="text-muted m-t-20">longitude</h5>
                            <h5 class="m-b-0" id="labelLng">Esperando...</h5>
                          </li>
                          <li>
                          </li>
                          <li>
                            <h5 class="text-muted m-t-20">Latitude</h5>
                            <h5 class="m-b-0" id="labelLat">Esperando...</h5>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
    				</div>
    			</div>
  		  </div>
      </div>

      <!--Map-->
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-border panel-custom">
            <div class="panel-heading">
              <h3 class="panel-title">Ubicacion del Mapa</h3>
            </div>
            <div class="panel-body">
                <div id="map" style="width:100%; height:500px"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

  @include('general.chat.main_chat')
  @include('templates.administrator.components.modal')
@endsection

@section('scripts')
  @include('templates.administrator.components.footer')

  {!! Html::script('assets/own/js/vue.js') !!}
  {!! Html::script('assets/own/dist/sweetalert.min.js') !!}
  {!! Html::script('assets/plugins/moment/moment.js') !!}
  {!! Html::script('assets/pages/jquery.chat.user.js') !!}
  {!! Html::script('assets/own/js/chat/module_agent.js') !!}
  {!! Html::script('assets/own/js/chat/agent.js') !!}
  {!! Html::script('assets/own/js/chat/vue_chat_agent.js') !!}
  {!! Html::script('assets/own/js/mapAgent.js') !!}
@endsection
