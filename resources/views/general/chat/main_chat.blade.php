<div class="row" v-show='chatview'>
    <div class="col-md-2 show-chat">
        <button class="btn btn-danger" @click="showchat">
            <i class="fa fa-sign-out" aria-hidden="true"></i>
        </button>
        <button class="btn btn-warning" @click="activeAlert">
            <i class="fa fa-volume-up" aria-hidden="true"></i>
        </button>
    </div>
    <div class="col-md-8">
        <div class="panel panel-border panel-custom">
            <div class="panel-heading">
                <h3 class="panel-title">Chat</h3>
            </div>
            <div class="panel-body">
                <div class="col-md-12" id="myChat">
                    <div class="card-box">
                        <h4 class="m-t-0 m-b-20 header-title"><b>Central</b></h4>

                        <div class="chat-conversation">
                            <ul class="conversation-list nicescroll">
                            </ul>
                            <div class="row">
                                <div class="col-sm-9 chat-inputbar">
                                    <textarea rows="4" cols="50" class="form-control chat-input" placeholder="Escriba un mensaje" id="txtMessage" maxlength="250"></textarea>
                                </div>
                                <div class="col-sm-3 chat-send">
                                    <button type="submit" class="btn btn-md btn-info btn-block waves-effect waves-light" id="btnSend">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<audio id="soundAppAlert" src="{{ url('assets/own/js/chat/alert.mp3') }}"></audio>