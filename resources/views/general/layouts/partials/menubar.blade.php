<!-- ========== Left Sidebar Start ========== -->

<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">

        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>
                <li class="text-muted menu-title">Navigation</li>
                <li class="@yield('dashboardMenu', 'default') has_sub">
                    
                </li>
                
                <!--Fin de alertas  -->
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
  @include('templates.administrator.components.modal')


<!-- Left Sidebar End -->
