<input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

<div class="form-group">
    {!! Form::label('quantity', 'Cantidad*', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-8">
        {!! Form::number('quantity', null, ['class' => 'form-control', 'id' => 'quantity', 'required' => 'required', 'placeholder' => 'Cantidad de Heridos/Muertos','data-parsley-type' => "digits","data-parsley-type-message" => "Escriba un numero", "data-parsley-required-message" => "Escriba la Cantidad"]) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('type', 'Tipo de Persona*', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-8">
        {!! Form::select('type', config('personType'), null, ['class' => 'form-control', 'id' => 'personType', 'required' => 'required', "data-parsley-required-message" => "Escoja una Opción"]) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('type', 'Tipo de Alerta*', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-8">
        {!! Form::select('type', config('alertType'), null, ['class' => 'form-control', 'id' => 'alertType', 'required' => 'required', "data-parsley-required-message" => "Escoja una Opción"]) !!}
    </div>
</div>



<div class="form-group">
    {!! Form::label('address', 'Dirección*', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-8">
        {!! Form::text('address', null, ['class' => 'form-control', 'id' => 'address', 'required' => 'required', 'placeholder' => 'Dirección', "data-parsley-required-message" => "Escriba la Dirección"]) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('cause', 'Causa*', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-8">
        {!! Form::text('cause', null, ['class' => 'form-control', 'id' => 'cause', 'placeholder' => 'Causa','required' => 'required',"data-parsley-required-message" => "Escriba la Causa"]) !!}
    </div>
</div>
