<input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
<div class="form-group">
    {!! Form::label('description', 'Descripcion*', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::textarea('description', null, ['size' => '30x5','class' => 'form-control', 'id' => 'description', 'required' => 'required', 'placeholder' => 'Ejemplo:Descripcion de la Alerta', "data-parsley-required-message" => "Este campo no puede ir vacio"]) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('address', 'Dirección*', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('description', null, ['class' => 'form-control', 'id' => 'address', 'required' => 'required', 'placeholder' => 'Dirección de la alerta', "data-parsley-required-message" => "Escriba la dirección"]) !!}
    </div>
</div>
