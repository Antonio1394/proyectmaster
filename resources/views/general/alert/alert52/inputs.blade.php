<input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
<div class="form-group">
    {!! Form::label('description', 'Motivo*', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::textarea('description', null, ['size' => '30x5','class' => 'form-control', 'id' => 'description', 'required' => 'required', 'placeholder' => 'Ejemplo:Descripcion de la Alerta', "data-parsley-required-message" => "Este campo no puede ir vacio"]) !!}
    </div>
</div>
