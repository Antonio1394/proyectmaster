@extends('general.layouts.principal')
@section('title', 'Agente')
@section('alertMenu', 'active')
@section('styles')
    {!! Html::style('assets/own/dist/dataTables.bootstrap.min.css') !!}
    {!! Html::style('assets/own/dist/responsive.bootstrap.min.css') !!}
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
			<div class="panel panel-border panel-custom">
				<div class="panel-heading">
					<h3 class="panel-title">Alertas</h3>
				</div>
				<div class="panel-body">
                    <div class="div-btn-new">
                        <button class="btn btn-info waves-effect waves-light loadModal" data-toggle="modal" data-target="#modal-maintenances" data-url="/user/alerts/create" data-title="Generar Alerta">
                            <i class="fa fa-plus m-r-5"></i>
                            <span>Nuevo</span>
                        </button>
                    </div>
                    <table id="datatable" class="table table-striped table-bordered display responsive no-wrap"  width="100%">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Descripcion</th>
                                <th>Dirección</th>
                                <th>Tipo de Alerta</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($alerts as $key => $value)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $value->description }}</td>
                                    <td>{{ $value->address }}</td>
                                    <td>{{ $value->alert_type }}</td>
                                    <td>
                                      <button type="button" name="button"></button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
				</div>
			</div>
		</div>
    </div>
    @include('templates.administrator.components.modal')
@endsection

@section('scripts')
    {!! Html::script('assets/plugins/datatables/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/plugins/datatables/dataTables.bootstrap.js') !!}
    {!! Html::script('assets/own/dist/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/own/dist/responsive.bootstrap.min.js') !!}
    {!! Html::script('assets/plugins/parsleyjs/dist/parsley.min.js') !!}

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').DataTable( {
                language: {
                    url: '/assets/own/json/spanish.json'
                }
            } );
        } );
    </script>
@endsection
