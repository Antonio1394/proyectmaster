<input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

<div class="form-group">
    {!! Form::label('type', 'Tipo*', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-8">
        {!! Form::select('mode', config('mode'), null, ['class' => 'form-control', 'id' => 'mode', 'required' => 'required', "data-parsley-required-message" => "Escoja una Opción"]) !!}
    </div>
</div>



<div class="form-group">
    {!! Form::label('type', 'Tipo de Vehículo*', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-8">
        {!! Form::select('type', config('typeCar'), null, ['class' => 'form-control', 'id' => 'type', 'required' => 'required', "data-parsley-required-message" => "Escoja una Opción"]) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('brand', 'Marca del Vehículo*', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-8">
        {!! Form::text('brand', null, ['class' => 'form-control', 'id' => 'brand', 'required' => 'required', 'placeholder' => 'Marca', "data-parsley-required-message" => "Escriba la marca"]) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('color', 'Color del Vehículo*', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-8">
        {!! Form::text('color', null, ['class' => 'form-control', 'id' => 'color', 'required' => 'required', 'placeholder' => 'Color', "data-parsley-required-message" => "Escriba el Color"]) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('plate', 'Placa Vehículo', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-8">
        {!! Form::text('plate', null, ['class' => 'form-control', 'id' => 'plate', 'placeholder' => 'Placa, si no lo recuerda, dejar en blanco']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('address', 'Dirección*', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-8">
        {!! Form::text('address', null, ['class' => 'form-control', 'id' => 'address', 'required' => 'required', 'placeholder' => 'Dirección de la alerta', "data-parsley-required-message" => "Escriba la dirección"]) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('person', 'Denunciante*', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-8">
        {!! Form::text('person', null, ['class' => 'form-control', 'id' => 'person', 'placeholder' => 'Nombre de la persona que denuncia', 'required' => 'required',"data-parsley-required-message" => "Escriba el Denunciate"]) !!}
    </div>
</div>
