<input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

<div class="form-group">
    {!! Form::label('roberry', 'Robo a:*', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('roberry', null, ['class' => 'form-control', 'id' => 'roberry', 'required' => 'required', 'placeholder' => 'Ejemplo: persona, local', "data-parsley-required-message" => "Este campo no puede ir vacio"]) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('individual', 'Individuo*', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::textarea('individual', null, ['size' => '30x5','class' => 'form-control', 'id' => 'individual', 'required' => 'required', 'placeholder' => 'Definicion del individuo que cometio el robo', "data-parsley-required-message" => "Este campo no puede ir vacio"]) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('address', 'Dirección*', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('address', null, ['class' => 'form-control', 'id' => 'address', 'required' => 'required', 'placeholder' => 'Dirección Física', "data-parsley-required-message" => "Escriba la Dirección"]) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('object', 'Definicion de lo Robado*', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::textarea('object', null, ['size' => '30x5','class' => 'form-control', 'id' => 'object', 'required' => 'required', 'placeholder' => 'Definición de lo Robado, Mercaderia, objeto en especifico.', "data-parsley-required-message" => "Este campo no puede ir vacio"]) !!}
    </div>
</div>


<div class="form-group">
    {!! Form::label('person', 'Denunciante*', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-8">
        {!! Form::text('person', null, ['class' => 'form-control', 'id' => 'person', 'placeholder' => 'Nombre de la persona que denuncia', 'required' => 'required',"data-parsley-required-message" => "Escriba el Denunciate"]) !!}
    </div>
</div>
