@extends('general.layouts.principal')
@section('title', 'Agente')
@section('alertMenu', 'active')

@section('styles')
    {!! Html::style('assets/own/dist/dataTables.bootstrap.min.css') !!}
    {!! Html::style('assets/own/dist/responsive.bootstrap.min.css') !!}
    {!! Html::style('assets/own/dist/sweetalert.css') !!}
    {!! Html::script('assets/own/js/socket.io.js') !!}
@endsection

@section('content')
  <div class="main-body" v-show="!chatview">
    <div class="show-chat">
      <button class="btn btn-success" @click="showchat">
          <i class="fa fa-commenting-o" aria-hidden="true"></i>
      </button>
      <button v-if="varCheckAlert" class="btn btn-danger" @click="checkAlert">
          <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
      </button>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Generador de Preliminares</h4>
          <p class="text-muted page-title-alt">Escoja el Tipo de Preliminar</p>
        </div>
      </div>

      <div class="row">
        <a class="loadModal" data-toggle="modal" data-target="#modal-maintenances" data-url="/user/alerts/create" data-title="Generar Alerta">
        <div class="col-lg-5 col-sm-4">
          <div class="widget-panel widget-style-2 bg-info">
            <i class="fa fa-bolt"></i>
            <h4 class="m-0 text-dark counter font-600">Alerta General</h4>
            <div class="text-muted m-t-5">Alerta de Cualquier tipo</div>
          </div>
        </div>
        </a>

        <a class="loadModal" data-toggle="modal" data-target="#modal-maintenances" data-url="/user/alerts/alertsCarCreate" data-title="Generar Alerta">
        <div class="col-lg-5 col-sm-4">
          <div class="widget-panel widget-style-2 bg-info">
            <i class=" md-local-car-wash"></i>
            <h4 class="m-0 text-dark counter font-600">Alerta Vehículo</h4>
            <div class="text-muted m-t-5">Robo de vehículo de cualquier tipo</div>
          </div>
        </div>
        </a>
      </div>

      <div class="row">
        <a class="loadModal" data-toggle="modal" data-target="#modal-maintenances" data-url="/user/alerts/alertStoleCreate" data-title="Generar Alerta">
        <div class="col-lg-5 col-sm-4">
          <div class="widget-panel widget-style-2 bg-info">
            <i class=" md-remove-circle-outline"></i>
            <h4 class="m-0 text-dark counter font-600">Alerta Robo</h4>
            <div class="text-muted m-t-5">Robo General</div>
          </div>
        </div>
        </a>

        <a class="loadModal" data-toggle="modal" data-target="#modal-maintenances" data-url="/user/alerts/alert52Create" data-title="Generar Alerta">
        <div class="col-lg-5 col-sm-4">
          <div class="widget-panel widget-style-2 bg-danger">
            <i class="ion-ios7-bell-outline"></i>
            <h4 style="color:white;" class="m-0  counter font-600">Apoyo 5-2</h4>
            <div class="text-muted m-t-5">Alerta de emergencia</div>
          </div>
        </div>
        </a>
      </div>


      <div class="row">
        <a class="loadModal" data-toggle="modal" data-target="#modal-maintenances" data-url="/user/alerts/alertDeathCreate" data-title="Generar Alerta">
        <div class="col-lg-5 col-sm-4">
          <div class="widget-panel widget-style-2 bg-danger">
            <i class=" md-report-problem"></i>
            <h4 style="color:white;" class="m-0  counter font-600">Alerta Heridos/Fallecidos</h4>
            <div class="text-muted m-t-5">Asesinato o heridos</div>
          </div>
        </div>
        </a>
      </div>
    </div>
  </div>

  @include('general.chat.main_chat')
  @include('templates.administrator.components.modal')
@endsection

@section('scripts')
    {!! Html::script('assets/plugins/datatables/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/plugins/datatables/dataTables.bootstrap.js') !!}
    {!! Html::script('assets/own/dist/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/own/dist/responsive.bootstrap.min.js') !!}
    {!! Html::script('assets/plugins/parsleyjs/dist/parsley.min.js') !!}

    @include('templates.administrator.components.footer')

    {!! Html::script('assets/own/js/vue.js') !!}
    {!! Html::script('assets/own/dist/sweetalert.min.js') !!}
    {!! Html::script('assets/plugins/moment/moment.js') !!}
    {!! Html::script('assets/pages/jquery.chat.user.js') !!}
    {!! Html::script('assets/own/js/chat/module_agent.js') !!}
    {!! Html::script('assets/own/js/chat/agent.js') !!}
    {!! Html::script('assets/own/js/chat/vue_chat_agent.js') !!}

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').DataTable( {
                language: {
                    url: '/assets/own/json/spanish.json'
                }
            } );
        } );
    </script>
@endsection
