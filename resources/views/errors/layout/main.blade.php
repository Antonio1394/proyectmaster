@include('templates.errors.partials.head')
<body class="@yield('class')">
    <script>var init = [];</script>

                @yield('content')

    @include('templates.errors.partials.scripts')
</body>
</html>