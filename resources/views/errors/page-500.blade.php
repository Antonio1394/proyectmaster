@extends('errors.layout.main')

@section('title', 'Error 500')

@section('class', 'page-500')

@section('content')

    <div class="header">
        <a href="{{ url('/') }}" style="text-decoration: none" class="logo">
            <h3 class="text-center">
                <img src="/assets/images/logo_dark.png" alt=""><strong class="text-custom"> PNC</strong>
            </h3>
        </a>
    </div>

    <br>
    <br>
    <br>
    <div class="error-code">500</div>

    <div class="error-text">
        <span class="oops">OUCH!</span><br>
        <span class="hr"></span>
        <br>
        Algo no está del todo bien
        <br>
        <span class="solve">Esperamos resolverlo en breve</span>
    </div> <!-- / .error-text -->
@endsection