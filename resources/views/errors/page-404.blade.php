@extends('errors.layout.main')

@section('title', 'Error 404')

@section('class', 'page-404')

@section('content')

    <div class="header">
        <a href="{{ url('/') }}" style="text-decoration: none" class="logo">
            <h3 class="text-center">
                <img src="/assets/images/logo_dark.png" alt=""><strong class="text-custom"> PNC</strong>
            </h3>
        </a>
    </div>
    <br>
    <br>
    <br>
    <div class="error-code">404</div>

    <div class="error-text">
        <span class="oops">OOPS!</span><br>
        <span class="hr"></span>
        <br>
        Algo salió mal, o la página no existe ... todavía
    </div>
@endsection







    