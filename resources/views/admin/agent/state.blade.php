{!!Form::open(['url' =>'admin/agents/changeState/'. $id.'/'.$value, 'method' => 'PUT'])!!}
    <h4>
        ¿Desea Realizar este proceso?
    </h4>
    <div class="modal-footer">
        {!!Form::submit('Si', ['class' => 'btn btn-success'])!!}
        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
    </div>
{!!Form::close()!!}

<script type="text/javascript">
    $('#modal-maintenances form').submit(function(e){
        $("#modal-maintenances .btn-success").prop('disabled', true);
    });
</script>
