@extends('admin.layouts.principal')

@section('title', 'Administrador')

@section('agentMenu', 'active')

@section('styles')
    {!! Html::style('assets/own/dist/dataTables.bootstrap.min.css') !!}
    {!! Html::style('assets/own/dist/responsive.bootstrap.min.css') !!}
    {!! Html::style('assets/own/dist/sweetalert.css') !!}
    {!! Html::script('assets/own/js/socket.io.js') !!}
@endsection

@section('content')
    <div class="main-body" v-show="!chatview">
        <div class="row">
            <div class="col-lg-12">
    			<div class="panel panel-border panel-custom">
    				<div class="panel-heading">
    					<h3 class="panel-title">Elementos Policiales</h3>
    				</div>
    				<div class="panel-body">
                        <div class="div-btn-new">
                            <button class="btn btn-info waves-effect waves-light loadModal" data-toggle="modal" data-target="#modal-maintenances" data-url="/admin/agents/create" data-title="Crear Elemento Policial">
                                <i class="fa fa-plus m-r-5"></i>
                                <span>Nuevo</span>
                            </button>
                        </div>
                        <table id="datatable" class="table table-striped table-bordered display responsive no-wrap"  width="100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nombre</th>
                                    <th>DPI</th>
                                    <th>Teléfono</th>
                                    <th>Acciones</th>
                                    <th>Estados</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($agents as $key => $value)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $value->last_name }} {{ $value->first_name }}</td>
                                        <td>{{ $value->dpi }}</td>
                                        <td>{{ $value->phone }}</td>
                                        <td class="text-center">
                                            <button class="btn btn-icon waves-effect btn-default waves-light loadModal" data-toggle="modal" data-target="#modal-maintenances" data-url="/admin/agents/{{ $value->id }}" data-title="Ver Agente">
                                                <i class="fa fa fa-search" aria-hidden="true"></i>
                                            </button>
                                            <button class="btn btn-icon waves-effect waves-light btn-primary loadModal" data-toggle="modal" data-target="#modal-maintenances" data-url="/admin/agents/{{ $value->id }}/edit" data-title="Actualizar Agente">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </button>
                                            <button class="btn btn-icon waves-effect waves-light btn-danger loadModal" data-toggle="modal" data-target="#modal-maintenances" data-url="/admin/agents/showdelete/{{ $value->id }}" data-title="Eliminar Agente">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </button>
                                        </td>
                                        <td>
                                            <button class="btn btn-icon waves-effect waves-light btn-purple loadModal" data-toggle="modal" data-target="#modal-maintenances" data-url="/admin/users/save/{{ $value->id }}" data-title="Crear Usuario ">
                                                <i class="ion-android-contact" aria-hidden="true"></i>
                                            </button>

                                            <button class="btn btn-icon waves-effect waves-light btn-white loadModal" data-toggle="modal" data-target="#modal-maintenances" data-url="/admin/agents/showState/{{ $value->id }}/1" data-title="Suspender Agente ">
                                                <i class="ion-ios7-minus-outline " aria-hidden="true"></i>
                                            </button>

                                            <button class="btn btn-icon waves-effect waves-light btn-warning loadModal" data-toggle="modal" data-target="#modal-maintenances" data-url="/admin/agents/showState/{{ $value->id }}/2" data-title="Confirmar Descanso de Agente">
                                                <i class="ion-ios7-moon " aria-hidden="true"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
    				</div>
    			</div>
    		</div>
        </div>
    </div>

    @include('templates.administrator.components.modal')
@endsection

@section('scripts')
    @include('templates.administrator.components.footer')

    {!! Html::script('assets/plugins/datatables/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/plugins/datatables/dataTables.bootstrap.js') !!}
    {!! Html::script('assets/own/dist/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/own/dist/responsive.bootstrap.min.js') !!}
    {!! Html::script('assets/plugins/parsleyjs/dist/parsley.min.js') !!}

    {!! Html::script('assets/own/dist/sweetalert.min.js') !!}
    {!! Html::script('assets/plugins/moment/moment.js') !!}
    {!! Html::script('assets/pages/jquery.chat.admin.js') !!}
    {!! Html::script('assets/own/js/vue.js') !!}
    {!! Html::script('assets/own/js/vue-resource.js') !!}
    {!! Html::script('assets/own/js/chat/module_admin.js') !!}
    {!! Html::script('assets/own/js/chat/admin.js') !!}
    {!! Html::script('assets/own/js/alertsRealTimeAdmin.js') !!}

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').DataTable( {
                language: {
                    url: '/assets/own/json/spanish.json'
                }
            } );
        } );
    </script>
@endsection
