<input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
<div class="form-group">
    {!! Form::label('first_name', 'Nombres*', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('first_name', null, ['class' => 'form-control', 'id' => 'first_name', 'required' => 'required', 'placeholder' => 'Nombres', "data-parsley-required-message" => "Escriba los nombres"]) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('last_name', 'Apellidos*', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('last_name', null, ['class' => 'form-control', 'id' => 'last_name', 'required' => 'required', 'placeholder' => 'Apellidos', "data-parsley-required-message" => "Escriba los apellidos"]) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('dpi', 'DPI*', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('dpi', null, ['class' => 'form-control', 'id' => 'dpi', 'required' => 'required', 'placeholder' => 'DPI', "data-parsley-required-message" => "Escriba el DPI"]) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('age', 'Edad*', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('age', null, ['class' => 'form-control', 'id' => 'age', 'required' => 'required', 'placeholder' => 'Edad', 'data-parsley-type' => "digits", "data-parsley-required-message" => "Escriba la edad", "data-parsley-type-message" => "Escriba un numero"]) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('phone', 'Teléfono*', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('phone', null, ['class' => 'form-control', 'id' => 'phone', 'required' => 'required', 'placeholder' => 'Teléfono', 'data-parsley-minlength' => "8", "data-parsley-minlength-message" => "Escriba 8 digitos", "data-parsley-maxlength" => "8", "data-parsley-maxlength-message" => "Escriba 8 digitos", "data-parsley-type" => "digits", "data-parsley-type-message" => "Escriba numeros", "data-parsley-required-message" => "Escriba el teléfono"]) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('address', 'Destino*', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('address', null, ['class' => 'form-control', 'id' => 'address', 'required' => 'required', 'placeholder' => 'Dirección', "data-parsley-required-message" => "Escriba la dirección"]) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('rank', 'Rango*', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::select('rank', config('rank'), null, ['class' => 'form-control', 'id' => 'rank', 'required' => 'required', "data-parsley-required-message" => "Escriba el rango"]) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('image', 'Imagen*', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        <div class="bootstrap-filestyle input-group">
            {!! Form::text('textImage', null, ['class' => 'form-control input-sm', 'id' => 'textImage', "disabled" => ""]) !!}
            <span class="group-span-filestyle input-group-btn" tabindex="0">
                <label for="image" class="btn btn-default btn-sm">
                    <span class="icon-span-filestyle glyphicon glyphicon-folder-open"></span> <span class="buttonText">Seleccione imagen</span>
                </label>
            </span>
        </div>
        {!! Form::file('image', ["class" => "filestyle", "data-size" => "sm", "id" => "image", "tabindex" => "-1", "style" => "position: absolute; clip: rect(0px 0px 0px 0px);", "data-parsley-formatimage", "data-parsley-sizeimage"]) !!}
    </div>
</div>
<hr>
<div class="form-group">
    {!! Form::label('id_turn', 'Turno*', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::select('id_turn',$turn, null, ['class' => 'form-control','required' => 'required', "data-parsley-required-message" => "Seleccione un Turno"]) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('nip', 'NIP*', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('nip', null, ['class' => 'form-control', 'id' => 'nip', 'required' => 'required', 'placeholder' => 'NIP', 'data-parsley-type' => "digits", "data-parsley-required-message" => "Escriba el NIP del Agente", "data-parsley-type-message" => "Escriba un numero",'data-parsley-minlength' => "8","data-parsley-minlength-message" => "Escriba 8 digitos"]) !!}
    </div>
</div>
