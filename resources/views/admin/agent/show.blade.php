<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="inbox-widget nicescroll mx-box">
                <div class="inbox-item">
                    <p class="inbox-item-author">Rango</p>
                    <p class="inbox-item-text">{{ $dataShow->rank }}</p>
                    <p class="inbox-item-date"><i class="fa fa-list" aria-hidden="true"></i></p>
                </div>
                <div class="inbox-item">
                    <p class="inbox-item-author">Nombre</p>
                    <p class="inbox-item-text">{{ $dataShow->last_name }} {{ $dataShow->first_name }}</p>
                    <p class="inbox-item-date"><i class="fa fa-user" aria-hidden="true"></i></p>
                </div>
                <div class="inbox-item">
                    <p class="inbox-item-author">NIP</p>
                    <p class="inbox-item-text">{{ $dataShow->nip }}</p>
                    <p class="inbox-item-date"><i class="fa fa-list" aria-hidden="true"></i></p>
                </div>
                <div class="inbox-item">
                    <p class="inbox-item-author">Destino</p>
                    <p class="inbox-item-text">{{ $dataShow->address }}</p>
                    <p class="inbox-item-date"><i class="fa fa-map-marker" aria-hidden="true"></i></p>
                </div>
                <div class="inbox-item">
                    <p class="inbox-item-author">Teléfono</p>
                    <p class="inbox-item-text">{{ $dataShow->phone }}</p>
                    <p class="inbox-item-date"><i class="fa fa-phone" aria-hidden="true"></i></p>
                </div>
                <div class="inbox-item">
                    <p class="inbox-item-author">DPI</p>
                    <p class="inbox-item-text">{{ $dataShow->dpi }}</p>
                    <p class="inbox-item-date"><i class="fa fa-crosshairs" aria-hidden="true"></i></p>
                </div>
                <div class="inbox-item">
                    <p class="inbox-item-author">Edad</p>
                    <p class="inbox-item-text">{{ $dataShow->age }}</p>
                    <p class="inbox-item-date"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></p>
                </div>



                <div class="inbox-item">
                    <p class="inbox-item-author">Imagen</p>
                    <p class="inbox-item-text">
                        <img src="{{ url('assets/own/images/agents/' . $dataShow->image ) }}" alt="No se pudo cargar la imagen" class="modal-image">
                    </p>
                    <p class="inbox-item-date"><i class="fa fa-list" aria-hidden="true"></i></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
</div>
