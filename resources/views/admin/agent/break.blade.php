@extends('admin.layouts.principal')

@section('title', 'Administrador')

@section('breakMenu', 'active')

@section('styles')
    {!! Html::style('assets/own/dist/dataTables.bootstrap.min.css') !!}
    {!! Html::style('assets/own/dist/responsive.bootstrap.min.css') !!}
    {!! Html::style('assets/own/dist/sweetalert.css') !!}
    {!! Html::script('assets/own/js/socket.io.js') !!}
@endsection

@section('content')
    <div class="main-body" v-show="!chatview">
        <div class="show-chat">
            <button class="btn btn-success" @click="showchat">
                <i class="fa fa-commenting-o" aria-hidden="true"></i>
            </button>
            <button class="btn btn-warning" @click="activeAlert">
                <i class="fa fa-volume-up" aria-hidden="true"></i>
            </button>
            <button v-if="varCheckAlert" class="btn btn-danger" @click="checkAlert">
                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
            </button>
        </div>
        <div class="row">
            <div class="col-lg-12">
    			<div class="panel panel-border panel-custom">
    				<div class="panel-heading">
    					<h3 class="panel-title">Elementos Policiales De Descanso</h3>
    				</div>
    				<div class="panel-body">
                      <table id="datatable" class="table table-striped table-bordered display responsive no-wrap"  width="100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nombre</th>
                                    <th>DPI</th>
                                    <th>Teléfono</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($agents as $key => $value)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $value->last_name }} {{ $value->first_name }}</td>
                                        <td>{{ $value->dpi }}</td>
                                        <td>{{ $value->phone }}</td>
                                        <td class="text-center">
                                            <button class="btn btn-icon waves-effect btn-default waves-light loadModal" data-toggle="modal" data-target="#modal-maintenances" data-url="/admin/agents/{{ $value->id }}" data-title="Ver Agente">
                                                <i class="fa fa fa-search" aria-hidden="true"></i>
                                            </button>
                                            <button class="btn btn-icon waves-effect waves-light btn-primary loadModal" data-toggle="modal" data-target="#modal-maintenances" data-url="/admin/agents/showChange/{{ $value->id }}" data-title="Activar Agente ">
                                                <i class=" md-trending-up" aria-hidden="true"></i>
                                            </button>


                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
    				</div>
    			</div>
    		</div>
        </div>
    </div>

   
    @include('templates.administrator.components.modal')
@endsection

@section('scripts')
    @include('templates.administrator.components.footer')

    {!! Html::script('assets/plugins/datatables/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/plugins/datatables/dataTables.bootstrap.js') !!}
    {!! Html::script('assets/own/dist/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/own/dist/responsive.bootstrap.min.js') !!}
    {!! Html::script('assets/plugins/parsleyjs/dist/parsley.min.js') !!}

    {!! Html::script('assets/own/dist/sweetalert.min.js') !!}
    {!! Html::script('assets/plugins/moment/moment.js') !!}
    {!! Html::script('assets/pages/jquery.chat.admin.js') !!}
    {!! Html::script('assets/own/js/vue.js') !!}
    {!! Html::script('assets/own/js/vue-resource.js') !!}
    {!! Html::script('assets/own/js/chat/module_admin.js') !!}
    {!! Html::script('assets/own/js/chat/admin.js') !!}
    {!! Html::script('assets/own/js/alertsRealTimeAdmin.js') !!}

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').DataTable( {
                language: {
                    url: '/assets/own/json/spanish.json'
                }
            } );
        } );
    </script>
@endsection
