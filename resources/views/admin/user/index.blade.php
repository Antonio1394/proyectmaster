@extends('admin.layouts.principal')

@section('title', 'Administrador')

@section('userMenu', 'active')

@section('styles')
    {!! Html::style('assets/own/dist/dataTables.bootstrap.min.css') !!}
    {!! Html::style('assets/own/dist/responsive.bootstrap.min.css') !!}
    {!! Html::style('assets/own/dist/sweetalert.css') !!}
    {!! Html::script('assets/own/js/socket.io.js') !!}
@endsection

@section('content')
    <div class="main-body" v-show="!chatview">
        <div class="row">
            <div class="col-lg-12">
    			<div class="panel panel-border panel-custom">
    				<div class="panel-heading">
    					<h3 class="panel-title">Usuarios</h3>
    				</div>
    				<div class="panel-body">

                        <table id="datatable" class="table table-striped table-bordered display responsive no-wrap"  width="100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Usuario de Acceso</th>
                                    <th>Tipo de Usuario</th>
                                    <th>Elemento Policial</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($user as $key => $value)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $value->user }}</td>
                                        <td>{{ $value->user_type->type }}</td>
                                        <td>{{ $value->agent->first_name }} {{$value->agent->last_name}}</td>
                                        @if($value->status==1)
                                            <td class="text-center"><span class="label label-success">Activo</span></td>
                                            <td class="text-center">
                                                <button class="btn btn-icon waves-effect waves-light btn-danger loadModal" data-toggle="modal" data-target="#modal-maintenances" data-url="/admin/users/showDown/{{ $value->id }}" data-title="Dar de Baja">
                                                    <i class=" ti-angle-double-down" aria-hidden="true"></i>
                                                </button>
                                            </td>
                                        @else
                                          <td class="text-center"><span class="label label-danger">De Baja</span></td>
                                          <td class="text-center">
                                              <button class="btn btn-icon waves-effect btn-info waves-light loadModal" data-toggle="modal" data-target="#modal-maintenances" data-url="/admin/users/showUp/{{ $value->id }}" data-title="Dar de Alta">
                                                  <i class=" ti-angle-double-up" aria-hidden="true"></i>
                                              </button>
                                          </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
    				</div>
    			</div>
    		</div>
        </div>
    </div>

    
    @include('templates.administrator.components.modal')
@endsection

@section('scripts')
    @include('templates.administrator.components.footer')

    {!! Html::script('assets/plugins/datatables/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/plugins/datatables/dataTables.bootstrap.js') !!}
    {!! Html::script('assets/own/dist/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/own/dist/responsive.bootstrap.min.js') !!}
    {!! Html::script('assets/plugins/parsleyjs/dist/parsley.min.js') !!}

    {!! Html::script('assets/own/dist/sweetalert.min.js') !!}
    {!! Html::script('assets/plugins/moment/moment.js') !!}
    {!! Html::script('assets/pages/jquery.chat.admin.js') !!}
    {!! Html::script('assets/own/js/vue.js') !!}
    {!! Html::script('assets/own/js/vue-resource.js') !!}
    {!! Html::script('assets/own/js/chat/module_admin.js') !!}
    {!! Html::script('assets/own/js/chat/admin.js') !!}
    {!! Html::script('assets/own/js/alertsRealTimeAdmin.js') !!}

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').DataTable( {
                language: {
                    url: '/assets/own/json/spanish.json'
                }
            } );
        } );
    </script>
@endsection
