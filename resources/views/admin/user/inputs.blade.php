<input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
<input type="hidden" name="id_agent" id="idAgent" value="{{$id}}">

<div class="form-group">
    {!! Form::label('user', 'Usuario*', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('user', null, ['class' => 'form-control', 'id' => 'user', 'required' => 'required', 'placeholder' => 'Usuario', "data-parsley-required-message" => "Escriba el Usuario"]) !!}
    </div>
</div>

<div class="form-group">
      {!! Form::label('password', 'Password*', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
      {!! Form::password('password',  ['class' => 'form-control', 'id' => 'password', 'required' => 'required', 'placeholder' => 'Contraseña', 'data-parsley-minlength' => "8", "data-parsley-minlength-message" => "Escriba 8 Carácteres", "data-parsley-maxlength" => "10", "data-parsley-maxlength-message" => "Escriba 10 carácteres", "Escriba numeros", "data-parsley-required-message" => "Escriba la Contraseña"]) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('id_user_type', 'Tipo de Usuario*', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::select('id_user_type', $type, null, ['class' => 'form-control', 'id' => 'id_type', 'required' => 'required', "data-parsley-required-message" => "Escoja una Opción"]) !!}
    </div>
</div>
