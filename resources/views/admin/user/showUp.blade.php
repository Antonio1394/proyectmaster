{!!Form::open(['route' => ['admin/users/activate', $id], 'method' => 'PUT'])!!}
    <h4>
        ¿Desea Dar de alta este registro?
    </h4>
    <div class="modal-footer">
        {!!Form::submit('Si', ['class' => 'btn btn-success'])!!}
        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
    </div>
{!!Form::close()!!}

<script type="text/javascript">
    $('#modal-maintenances form').submit(function(e){
        $("#modal-maintenances .btn-success").prop('disabled', true);
    });
</script>
