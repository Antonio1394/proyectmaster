'use strict';

const express = require("express"),
		fs = require("fs"),
		debug = require('debug')('nodeserver:server'),
		app = express(),
		https = require('https'),
		mongoose = require('mongoose'),
		port = process.env.PORT || 5000,
		alerts = require("./modules/alerts"),
		chat = require("./modules/chat");

// const server = https.createServer({
// 					ca: fs.readFileSync('./cert/sf_bundle-g2-g1.crt'),
// 					key: fs.readFileSync('./cert/key.txt'),
// 					cert: fs.readFileSync('./cert/2edd583cf30ed02a.crt')
// 				}, app);
const server = require("http").createServer(app);
const io = require("socket.io").listen(server);

process.env.TZ = 'America/Guatemala';

// Configuracion de la base de datos mongoDB
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost/chat-sys-comisaria", (err, res) => {
	if(err) debug("Error: conectando a la DB: " + err);
	else debug("Conexión a la DB realizada");
});

// Acciones del servidor
alerts(server, io);
chat(io);

server.listen(port, () => {
	debug(`Servidro correindo en el puerto: ${port}`);
});

