module.exports = (server, io) => {
	const debug = require('debug')('nodeserver:alerts');
	Redis = require('ioredis');

	var redis = new Redis();


	redis.subscribe('alerts-channel', (err, count) => {
		if(err) debug('Error al conectar con el canal alerts-channel');
		else debug('Conectado al canal alerts-channel');
	});

	redis.on('message', (channel, message) => {
		debug("Nueva alerta recivida");

		message = JSON.parse(message);

		io.emit(channel + ':' + message.event, message.data);
	});
}