'use strict';

function getToDay() {
    let today = new Date(),
               day = today.getDate(),
               month = today.getMonth()+1,
               year = today.getFullYear(),
               hours = today.getHours(),
               minutes = today.getMinutes(),
               seconds = today.getSeconds();

    if(day < 10)
       day = '0' + day;
    if(month < 10)
       month = '0' + month;
    if(hours < 10)
       hours = '0' + hours;
    if(minutes < 10)
       minutes = '0' + minutes;


    return `${day}/${month}/${year} ${hours}:${minutes}`;
}

module.exports.getToDay = getToDay;