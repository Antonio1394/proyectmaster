'use strict';

module.exports = (io) => {
	const Conversation = require("../models/conversation"),
			Message = require("../models/message"),
			ToDay = require("./date"),
			debug = require('debug')('nodeserver:chat'),
			sanitize = require('mongo-sanitize'),
			ValidationCentral = require('../middlewares/validation_central');
	
	var central = { socket: null, user: {} },
		connectionsActive = [],
		connection_now = { conver_id: null };

	/**
	*
	* SOCKETS PARA LA CENTRAL
	*
	**/

	var socketCenter = io.of('/central');

	socketCenter.use(function(packet, next) {
		if (central.socket == null)
			next();
		else
			next(new Error("Ya existe una central conectada"));
	});

	socketCenter.on('connection', (socket) => {

		// sockets de la registro de la central
		socket.on('registerCentral', (data, fn) => {

			debug('socket central del server: ' + central.socket);
			debug("=== Socket registerCentral ===" + socket.id);
			central.socket = socket.id;
			central.user = {
				id: sanitize(data.user.id),
				name: sanitize(data.user.first_name) + ' ' + sanitize(data.user.last_name),
				dpi: sanitize(data.user.dpi)
			};

			Conversation.find().then((conversations) => {
				debug('Conversaciones:' + conversations);

				debug('===========================');
				socketsAgent.emit('refreshCentral', { central: central.socket });
		    	debug('Administrador Conectado a socket: ' + socket.id);
		    	debug('===========================');

				fn({ conversations, connectionsActive });

			}).catch((err) => {
				debug(`Error: ${err}`);
			});

		});

		// Crear conversacion

		socket.on('createConversation', (data, fn) => {

			Conversation.findOne({ id_user: data.data_agent.id_user }).then((conversation) => {
				if(conversation == null) {
					var conversation = Conversation();

					conversation.id_user = sanitize(data.data_agent.id_user);
					conversation.username = sanitize(data.data_agent.username);
					conversation.image = sanitize(data.data_agent.image);
					conversation.user_central = central.user;
					conversation.see = true;

					conversation.save().then((new_conversation) => {

						for(let index in connectionsActive) {

							if(connectionsActive[index].id_user == conversation.id_user) {
								if(socketsAgent.connected[connectionsActive[index].socketAgent])
									socketsAgent.connected[connectionsActive[index].socketAgent].emit('startConversation', { _id: new_conversation._id });
							}
							
						}

						fn({ error: false, new_conversation });
					}).catch((err) => {
						debug(`Error: ${err}`);
						fn({ error: true });
					});

				} else {
					fn({ error: true });
				}
			});

		});

		// Eliminar conversacion

		socket.on('deleteConversation', (data, fn) => {
			Message.remove({ conversation: data.conversation_now._id }).then(() => {
				Conversation.remove({ _id: data.conversation_now._id }).then(() => {

					for(let index in connectionsActive) {

						if(connectionsActive[index].id_user == data.conversation_now.id_user) {
							if(socketsAgent.connected[connectionsActive[index].socketAgent])
								socketsAgent.connected[connectionsActive[index].socketAgent].emit('deleteConversation');
						}
						
					}

					fn({ error: false });
				}).catch((err) => {
					debug(`Error: ${err}`);
					fn({ error: true });
				});
			}).catch((err) => {
				fn({ error: true });
			});
		});

		// Socket para cargar mensajes en la conversacion de la central
		socket.on('loadMessage', (data, fn) => {
			debug('=== Socket loadMessage ===');
			debug('Conversacion: ' + data.conversation);
			connection_now.conver_id = sanitize(data.conversation._id);

			Conversation.findByIdAndUpdate(connection_now.conver_id, { see: true }, {new: true}).then((conversation_up) => {
				
				Message.find({ conversation: data.conversation._id }).then((messages) => {
					debug('Mensajes: ' + messages);
					fn({ messages, connection: data.conversation, connectionsActive });
				}).catch((err) => {
					debug(`Error: ${err}`);
				});
				
			}).catch((err) => {
				debug(`Error: ${err}`);
			});

		});

		// Socket para enviar mensaje de la central a un agente
		socket.on('sendSMSAdminToUser', (data) => {
			debug('=== Socket sendSMSAdminToUser ===');
			debug('Connection: ' + data.connection);
			var message = Message();

			message.message = sanitize(data.chatText);
			message.date = ToDay.getToDay();
			message.conversation = sanitize(data.connection._id);
			message.central = true;

			message.save().then((new_message) => {
				debug('Nuevo mensaje: ');
				debug(new_message);
				for(let index in connectionsActive) {

					if(connectionsActive[index].id_user == data.connection.id_user) {
						if(socketsAgent.connected[connectionsActive[index].socketAgent])
							socketsAgent.connected[connectionsActive[index].socketAgent].emit('newMessageToAgent', { new_message });
					}
					
				}

			}).catch((err) => {
				debug(`Error: ${err}`);
			});
		});

		socket.on('changeConverNow', (data, fn) => {
			connection_now.conver_id = null;
			fn(true);
		});

		socket.on('disconnect', (data) => {
			central.socket = null;
        	connection_now.conver_id = null;
        	socketsAgent.emit('refreshCentral', { central: central.socket });
        	debug('*********** Se desconecto la central namspace ***********');
		});

		debug('Conectado a socket central');
	});

	/**
	*
	* Sockets para el agente
	*
	**/

	var socketsAgent = io.of('/agent');

	socketsAgent.use(function(packet, next) {

		let id_user_con = packet.request._query['id_user'],
			flag = true;

		for(var i in connectionsActive) {
    		if( connectionsActive[i].id_user == id_user_con) 
    			flag = false;
    	}

		if (flag)
			next();
		else
			next(new Error("Ya existe una sesion con este usuario"));
	});

	socketsAgent.on('connection', (socket) => {

		// Socket para registrar al agente
		socket.on('subscribe', (data, fn) => {
			debug('===== Socket subscribe =====');
			var connection = {};
            let id_user_clear = sanitize(data.id);

			connection.mySocket = socket.id;
            connection.central = central.socket;
            connection.id_user = id_user_clear;
            connection.username = sanitize(data.first_name) + ' ' + sanitize(data.last_name);
            connection.image = sanitize(data.image);

            connectionsActive.push({ socketAgent: socket.id, id_user: id_user_clear });


            Conversation.findOne({ id_user: id_user_clear }).then((conversation) => {
            	debug('===== Conversacion ====');
            	debug('Conversacion: ' + conversation);
				if(conversation == null) connection._id = null;
				else connection._id = conversation._id;

				debug('=== Id conversacion ===');
				debug(connection._id);

				Message.find({ conversation: connection._id }).then((messages) => {
					debug('Mensajes: ' + messages);
					
					if (socketCenter.connected[connection.central]) {
						socketCenter.connected[connection.central].emit('refreshActives', { connectionsActive });
					}

					if(messages == null) {
						// socket.emit('getMyId', { connection, messages: [] });
						fn({ connection, messages: [] });
					} else {
						fn({ connection, messages });
						// socket.emit('getMyId', { connection, messages });
					}


				}).catch((err) => {
					debug(`Error: ${err}`);
				});

				debug('Agente Conectado a socket: ' + socket.id);
            }).catch((err) => {
            	debug(`Error: ${error}`);
            });

		});

		// Socket para enviar mensaje del agente a la central
		socket.on('sendSMSUserToAdmin', (data) => {
			debug("=== Socket sendSMSUserToAdmin ==");
			let id_con_clear = sanitize(data.user._id);
			if ( id_con_clear == null) {
				var conversation = Conversation();

				conversation.id_user = sanitize(data.user.id_user);
				conversation.username = sanitize(data.user.username);
				conversation.image = sanitize(data.user.image);
				conversation.user_central = central.user;

				conversation.save().then((new_conversation) => {
					debug('Id de la nueva conversación: ');
					debug(new_conversation);
					var message = Message();

					message.message = sanitize(data.chatText);
					message.date = ToDay.getToDay();
					message.conversation = new_conversation._id;

					message.save().then((new_message) => {
						if (socketCenter.connected[central.socket]) {
							socketCenter.connected[central.socket].emit('newMessageToAdmin', { new_message, connection: new_conversation, new: true });
						}

						socket.emit('startConversation', { _id: conversation._id });
						
						debug('Nuevo mensaje: ');
						debug(new_message);
					}).catch((err) => {
						debug(`Error: ${err}`);
					});
				}).catch((err) => {
					debug(`Error: ${err}`);
				});
			} else {
				var message = Message();

				message.message = sanitize(data.chatText);
				message.date = ToDay.getToDay();
				message.conversation = id_con_clear;

				message.save().then((new_message) => {
					let seeBoolean = false;
					console.log(connection_now);
					if (socketCenter.connected[central.socket]) {
						if (connection_now.conver_id == id_con_clear) 
							seeBoolean = true;
					}

					Conversation.findByIdAndUpdate(id_con_clear, { see: seeBoolean }, {new: true})
					.then((conversation_up) => {

							if (socketCenter.connected[central.socket])
								socketCenter.connected[central.socket].emit('newMessageToAdmin', { new_message, connection: data.user, new: false });

					}).catch((err) => {
						debug(`Error: ${err}`);
					});

					debug('Nuevo mensaje: ' + new_message);
				}).catch((err) => {
					debug(`Error: ${err}`);
				});
			}
		});

		socket.on('disconnect', (data) => {
			debug('Se desconecto el agente');
			for(var i in connectionsActive) {
        		if( connectionsActive[i].socketAgent == socket.id) {
        			connectionsActive.splice(i, 1);
        		}
        	}

        	if (socketCenter.connected[central.socket]) {
				socketCenter.connected[central.socket].emit('refreshActives', { connectionsActive });
			}
		});

		debug('Se conecto un agente');

	});

	// Socket de conexión
	io.sockets.on('connection', (socket) => {

		// Socket para la desconexion de un usuario
        /*socket.on("disconnect", function (data) {
        	debug("**** Un usuario se desconecto ****")
            if(socket.id == central.socket) {
            	central.socket = null;
            	connection_now.conver_id = null;
            	io.sockets.emit('refreshCentral', { central: central.socket });
            	debug('*********** Se desconecto la central ***********');
            } else {
            	for(var i in connectionsActive) {
            		if( connectionsActive[i].socketAgent == socket.id) {
            			connectionsActive.splice(i, 1);
            		}
            	}

            	if (socketCenter.connected[central.socket]) {
					socketCenter.connected[central.socket].emit('refreshActives', { connectionsActive });
				}
            }
        });*/

		debug('Conectado a socket general');
	});
}