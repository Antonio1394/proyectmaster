const mongoose = require('mongoose'),
		Schema = mongoose.Schema;

var conversation_schema = new Schema({
	id_user: {
		type: String,
		required: [true, 'Campo requerido.'],
		validate: {
          validator: function(v) {
            return /^([0-9])*$/.test(v);
          },
          message: '{VALUE} este campo debe ser numerico!'
        },
		unique: [true, 'Este campo debe ser unico.']
	},
	username: {
		type: String,
		required: [true, 'Campo requerido.']
	},
	image: {
		type: String,
		required: [true, 'Campo requerido.']
	},
	user_central: {
		type: Object,
		required: [true, 'Campo requerido.']
	},
	see: {
		type: Boolean,
		required: [true, 'Campo requerido.'],
		default: false
	}
});

var Conversation = mongoose.model('conversations', conversation_schema);

module.exports = Conversation;