const mongoose = require('mongoose'),
		Schema = mongoose.Schema;

var message_schema = new Schema({
	message: {
		type: String,
		required: [true, 'Campo requerido.']
	},
	date: {
		type: String,
		required: [true, 'Campo requerido.']
	},
	conversation: {
		type: Schema.Types.ObjectId,
		ref: "conversations",
		required: [true, 'Campo requerido.']
	},
	central: {
		type: Boolean,
		default: false,
		required: [true, 'Campo requerido.']
	}
});

var Message = mongoose.model('messages', message_schema);

module.exports = Message;