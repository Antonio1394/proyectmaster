<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user')->unique();
            $table->string('password', 60);
            $table->boolean('status')->default(true);
            $table->integer('id_user_type')->unsigned();
            $table->bigInteger('id_agent')->unsigned();
            $table->string('latitude');
            $table->string('longitude');
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('id_user_type')
                    ->references('id')
                    ->on('user_types');

            $table->foreign('id_agent')
                    ->references('id')
                    ->on('agents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
