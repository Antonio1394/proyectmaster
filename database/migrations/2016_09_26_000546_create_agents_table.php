<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('dpi')->unique();
            $table->integer('status')->default(0);
            $table->integer('age');
            $table->string('phone', 8);
            $table->string('address');
            $table->enum('rank', ['Comisario', 'Inspector', 'Oficial']);
            $table->string('image')->default('default.png');
            $table->bigInteger('nip');
            $table->bigInteger('id_turn')->unsigned();
            $table->timestamps();
            $table->foreign('id_turn')
                  ->references('id')
                  ->on('shifts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('agents');
    }
}
