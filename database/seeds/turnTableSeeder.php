<?php

use Illuminate\Database\Seeder;
use App\Models\Turn;
class turnTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $turn=new Turn;
        $turn->description="Central";
        $turn->state=1;
        $turn->save();

        $turn=new Turn;
        $turn->description="Turno1";
        $turn->state=1;
        $turn->save();

        $turn=new Turn;
        $turn->description="Turno2";
        $turn->state=1;
        $turn->save();
    }
}
