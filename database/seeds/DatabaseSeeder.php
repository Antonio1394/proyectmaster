<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(turnTableSeeder::class);
        $this->call(AgentsTableSeeder::class);
        factory('App\Models\Agent', 150)->create();
        $this->call(UserTypesTableSeeder::class);
        $this->call(UsersTableSeeder::class);


        Model::reguard();
    }
}
