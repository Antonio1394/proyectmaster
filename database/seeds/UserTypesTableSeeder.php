<?php

use Illuminate\Database\Seeder;
use App\Models\UserType;

class UserTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = new UserType;
        $type->type = 'Central';
        $type->save();

        $type = new UserType;
        $type->type = 'Patrullero';
        $type->save();
    }
}
