<?php

use Illuminate\Database\Seeder;
use App\Models\Agent;

class AgentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agent = new Agent;
        $agent->first_name = 'Rony';
        $agent->last_name = 'González';
        $agent->dpi = '2306407531001';
        $agent->age = 32;
        $agent->phone = '58758399';
        $agent->address = 'Retalhuelu';
        $agent->rank = 'Oficial';
        $agent->id_turn=1;
        $agent->save();

        $agent = new Agent;
        $agent->first_name = 'Sarai';
        $agent->last_name = 'Escobar';
        $agent->dpi = '2306407531002';
        $agent->age = 32;
        $agent->phone = '99385785';
        $agent->address = 'Retalhuelu';
        $agent->rank = 'Oficial';
        $agent->id_turn=2;
        $agent->save();
    }
}
