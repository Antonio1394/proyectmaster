<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Agent::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->name,
        'last_name'  => $faker->name,
        'dpi'        => $faker->e164PhoneNumber,
        'status'     => $faker->numberBetween($min = 0, $max = 2),
        'age'        => $faker->numberBetween($min = 15, $max = 99),
        'phone'      => $faker->PhoneNumber,
        'nip'        =>$faker->numberBetween($min = 10000, $max = 50000),
        'address'    => $faker->address,
        'id_turn'    =>$faker->numberBetween($min = 2, $max = 3),
        'rank'       =>$faker->randomElement($array = array ('Comisario','Inspector','Oficial'))
    ];
});
