<?php

return [
    ''             => 'Seleccione un tipo',
    'Motocicleta'  => 'Motocicleta',
    'Sedán'        => 'Sedán',
    'Pick Up'      => 'Pick Up',
    'Camioneta'    => 'Camioneta',
    'Camionetilla' => 'Camionetilla',
    'Camión'       => 'Camión',
    'Bus'          => 'Bus',
    'MotoTaxi'     => 'MotoTaxi',
    'Otro'         => 'Otro',
];
