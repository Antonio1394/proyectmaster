<?php

return [
    ''                => 'Seleccione un rango',
    'Agente'          => 'Agente',
    'Sub-Inspector'   => 'Sub-Inspector',
    'Inspector'       => 'Inspector',
    'Oficial Tercero' => 'Oficial Tercero',
    'Oficial Primero' => 'Oficial Primero',
    'Sub-Comisario'   => 'Sub-Comisario',
    'Comisario'       => 'Comisario',
];
