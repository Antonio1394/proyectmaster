<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user', 'password', 'status', 'id_user_type', 'id_agent','longitude','latitude'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public $relations = ['user_type', 'agent'];

    public function user_type()
    {
        return $this->belongsTo('App\Models\UserType', 'id_user_type');
    }

    public function agent()
    {
        return $this->belongsTo('App\Models\Agent', 'id_agent');
    }

    public function SetPasswordAttribute($value)
    {
        if ( !empty($value) ) {
            $this->attributes['password'] = bcrypt($value);
        }
    }

    public function is($type)
    {
        return $this->id_user_type === $type;
    }
}
