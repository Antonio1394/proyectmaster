<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $table = 'agents';

    protected $fillable = ['first_name', 'last_name', 'dpi', 'age', 'phone', 'status','address', 'email','id_turn', 'rank', 'image'];

    public $relations = ['users','shifts'];

    public function users()
    {
        return $this->hasMany('App\User', 'id_agent');
    }

    public function shifts()
    {
      return $this->belongsTo('App\Models\Turn','id_turn');
    }
}
