<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Turn extends Model
{
    protected $table='shifts';

    protected $fillable=['description','state'];

    public $relations=['agents'];

    public function agents()
    {
        return $this->hasMany('App\Agent','id_turn');
    }

}
