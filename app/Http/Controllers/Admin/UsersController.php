<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\UserType;
use App\Models\Agent;

use JavaScript;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        JavaScript::put([
            'user_agent' => $request->user()->agent
        ]);

        $user=User::where('id_agent','!=',Auth()->user()->id)->orderBy('id','desc')->get();
        return view('admin.user.index',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $type = $this->fillCombo(UserType::all(), 'type');
        $number=User::where('id_agent','=',$id)->count();
        if($number>0){
            return "Este Agente ya pertenece a un Usuario";
        }else{
            $user=Agent::findOrFail($id);
            if(empty($user))
            abort(404);
            return view('admin.user.create',compact('id','type'));
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            User::create($request->all());
            return redirect('admin/users')->with('message','Registro creado correctamente.');
        } catch (\Exception $e) {
            return redirect()->back()->with("error", "No se pudo realizar la acción.");

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try {
          $user=User::findOrFail($id);
          $user->status=0;
          $user->save();
          return redirect('admin/users')->with("message", "Registro Dado de baja correctamente.");
      } catch (\Exception $e) {
          return redirect('admin/users')->with("error", "No se pudo realizar la acción.");
      }
    }

    public function showUp($id)
    {
        if(User::where('id',$id)->count()!=0)
          return view('admin.user.showUp')->with('id',$id);
        else
          abort(404);
    }

    public function showDown($id)
    {
        if(User::where('id',$id)->count()!=0)
          return view('admin.user.delete')->with('id',$id);
        else
          abort(404);


    }

    public function build($id)
    {
        try {
           $user=User::findOrFail($id);
           $user->status=1;
           $user->save();
           return redirect('admin/users')->with('message','Registro dado de Alta correctamente.');

        } catch (Exception $e) {
          return redirect('admin/users')->with('error','Error no se pudo generar la Acción.');

        }

    }

    public function verifycreate(Request $request)
    {
       $number = User::where('user', $request->dataArray['user'])->count();
       $response = ($number == 0) ? "ok" : "error";
       return $response;
    }

    private function fillCombo($data, $field)
    {
        $dataCombo = ['' => 'Seleccione una opción'];

        foreach ($data as $value) {
            //$dataCombo[$value->id] = $value->$field;
            $dataCombo[$value->id] = $value->$field;

        }

        return $dataCombo;
    }
}
