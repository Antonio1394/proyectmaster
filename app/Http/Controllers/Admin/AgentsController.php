<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\AgentRequest;
use App\Http\Requests\AgentEditRequest;
use App\Models\Agent;
use App\Models\Turn;
use App\User;

use Carbon\Carbon;

use JavaScript;

class AgentsController extends Controller
{
    private $path = 'assets/own/images/agents';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     //////0 activo
     //////1 suspendido
     //////2 descanso
    public function index(Request $request)
    {
      JavaScript::put([
        'user_agent' => $request->user()->agent
      ]);

      $agents = Agent::where('status',0)->orderBy('id', 'desc')->get();
      $users = Agent::orderBy('id', 'desc')->get();
      return view('admin.agent.index', compact('agents','users'));
    }

    public function showSuspend(Request $request)
    {
      JavaScript::put([
        'user_agent' => $request->user()->agent
      ]);

      $agents=Agent::where('status','=',1)->orderBy('id','desc')->get();
      return view('admin.agent.suspending',compact('agents'));
    }

    public function showBreak(Request $request)
    {
      JavaScript::put([
        'user_agent' => $request->user()->agent
      ]);

      $agents=Agent::where('status','=',2)->orderBy('id','desc')->get();
      return view('admin.agent.break',compact('agents'));
    }

    public function showState($id,$value)
    {
      return view('admin.agent.state',compact('id','value'));
    }

    public function changeState($id,$value)
    {
      try {
         $agent=Agent::findOrFail($id);
         $agent->status=$value;
         $agent->save();
         return redirect('admin/agents')->with('message','Se cambio el estado del Agente correctamente.');

      } catch (Exception $e) {
        return redirect('admin/agents')->with('error','Error no se pudo generar la Acción.');

      }
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $turn=$this->fillCombo(Turn::all(),'description');
        return view('admin.agent.create',compact('turn'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AgentRequest $request)
    {
        try {
            $agent = new Agent;
            $agent->first_name = $request->first_name;
            $agent->last_name = $request->last_name;
            $agent->dpi = $request->dpi;
            $agent->age = $request->age;
            $agent->nip = $request->nip;
            $agent->phone = $request->phone;
            $agent->id_turn=$request->id_turn;
            $agent->address = $request->address;
            $agent->image = $this->saveImage($request);
            $agent->rank = $request->rank;
            $agent->save();
            return redirect()->back()->with('message','Registro creado correctamente.');
        } catch (\Exception $e) {
            return redirect()->back()->with("error", "No se pudo realizar la acción.". $e->getMessage());
            echo $e;
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataShow = Agent::find($id);

        if( empty($dataShow) )
            abort(404);

        return view('admin.agent.show', compact('dataShow'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $turn=$this->fillCombo(Turn::all(),'description');
        $dataEdit = Agent::find($id);

        if( empty($dataEdit) )
            abort(404);

        return view('admin.agent.edit', compact('dataEdit','turn'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AgentEditRequest $request, $id)
    {
        try {
            $dataEdit = Agent::findOrFail($id);
            $dataEdit->first_name = $request->first_name;
            $dataEdit->last_name = $request->last_name;
            $dataEdit->dpi = $request->dpi;
            $dataEdit->nip = $request->nip;
            $dataEdit->id_turn=$request->id_turn;
            $dataEdit->age = $request->age;
            $dataEdit->phone = $request->phone;
            $dataEdit->address = $request->address;
            if( !empty($request->image) )
                $dataEdit->image = $this->editImage($request, $dataEdit);
            $dataEdit->rank = $request->rank;
            $dataEdit->save();

            return redirect()->back()->with('message','Registro Editado correctamente.');
        } catch (\Exception $e) {
            return redirect()->back()->with("error", "No se pudo realizar la acción.");
        }

    }

    /**
     * Funcion para mostrar la vista de eliminacion
     */
    public function showdelete($id)
    {
        if( Agent::where('id',$id)->count() != 0 )
          return view('admin.agent.delete')->with('id',$id);
        else
          abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = Agent::findOrFail($id);
            $this->deleteImage($data);
            Agent::findOrFail($id)->delete();
            return redirect()->back()->with("message", "Registro eliminado correctamente.");
        } catch (\Exception $e) {
            return redirect()->back()->with("error", "No se pudo realizar la acción.");
        }
    }

    /**
    * Funcion para verificar el dpi unico en empleados
    */

    public function showChange($id)
    {
        return view('admin.agent.activate')->with('id',$id);
    }

    public function build($id)
    {

        try {
           $user=Agent::findOrFail($id);
           $user->status=0;
           $user->save();
           return redirect('admin/agents')->with('message','Registro Activado correctamente.');

        } catch (Exception $e) {
          return redirect('admin/agents')->with('error','Error no se pudo generar la Acción.');

        }

    }
    public function verifycreate(Request $request)
    {
        $number = Agent::where('dpi', $request->dataArray['dpi'])->count();
        $response = ($number == 0) ? "ok" : "error";
        return $response;
    }

    public function verifyNip(Request $request)
    {
        $number = Agent::where('nip', $request->dataArray['nip'])->count();
        $response = ($number == 0) ? "ok" : "error";
        return $response;
    }

    public function verifyeditNip(Request $request)
    {
        $number = Agent::where('nip', $request->dataArray['nip'])
                          ->where('id', '!=', $request->dataArray['id'])
                          ->count();
        $response = ($number == 0) ? "ok" : "error";
        return $response;
    }

    /**
    * Funcion para verificar el dpi unico en empleados
    */
    public function verifyedit(Request $request)
    {
      $number = Agent::where('dpi', $request->dataArray['dpi'])
                        ->where('id', '!=', $request->dataArray['id'])
                        ->count();

      $response = ($number == 0) ? "ok" : "error";
      return $response;
    }

    private function saveImage($request)
    {
        if( !empty($request->image) ) {
            $extension = $request->image->getClientOriginalExtension();
            $fileName = rand(99,500) . rand(1,2222) . Carbon::now()->toTimeString() . "." . $extension;
            $request->image->move($this->path, $fileName);
            return $fileName;
        }

        return 'default.png';
    }

    private function editImage($request, $dataEdit)
    {
        if( !empty($request->image) ) {
            $this->deleteImage($dataEdit);
            return $this->saveImage($request);
        }

        return 'default.png';
    }

    private function deleteImage($data)
    {
        if( $data->image != 'default.png' )
            \File::delete($this->path . '/' . $data->image);
    }

    private function fillCombo($data, $field)
    {
        $dataCombo = ['' => 'Seleccione una opción'];

        foreach ($data as $value) {
            //$dataCombo[$value->id] = $value->$field;
            $dataCombo[$value->id] = $value->$field;

        }

        return $dataCombo;
    }
}
