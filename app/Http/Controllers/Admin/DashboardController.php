<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\User;
use App\Models\Agent;

use JavaScript;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
    	

        $numberUsers = User::where('status', 1)->count();

        $numberSuspended = Agent::where('status', 1)->count();
        $numberRest = Agent::where('status', 2)->count();

    	JavaScript::put([
            'user_agent' => $request->user()->agent
        ]);

        return view('admin.index', 
        		compact('numberUsers', 'numberSuspended', 'numberRest'));
    }
}
