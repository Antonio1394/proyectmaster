<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Alert;
use App\Models\AlertCar;
use App\Models\AlertStole;
use App\Models\Alert52;
use DB;

use JavaScript;

class HistorialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        JavaScript::put([
            'user_agent' => $request->user()->agent
        ]);

        return view('admin.historial.index');
    }

    public function getRecord(Request $request)
    {

      try {
        if(empty($request->start) or empty($request->end))
        {
          $data['alerts']=Alert::all();
          $data['alertsCar']=AlertCar::all();
          $data['alertStole']=AlertStole::all();
          $data['alert52']=Alert52::all();
          return \Response::json($data,200);
        }else {
          $data['alerts']=Alert::whereBetween('date',[$request->start,$request->end])
                                ->get();
          $data['alertsCar']=AlertCar::whereBetween('date',[$request->start,$request->end])
                                ->get();
          $data['alertStole']=AlertStole::whereBetween('date',[$request->start,$request->end])
                                ->get();
          $data['alert52']=Alert52::whereBetween('date',[$request->start,$request->end])
                                ->get();
          return \Response::json($data,200);
        }

      } catch (\Exception $e) {
        return $e->getMessage();
      }



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
