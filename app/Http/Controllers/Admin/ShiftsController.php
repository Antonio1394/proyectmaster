<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Turn;
use App\Models\Agent;
use App\User;

use JavaScript;

class ShiftsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        JavaScript::put([
            'user_agent' => $request->user()->agent
        ]);

        $turn=Turn::orderBy('id','desc')->get();
        return view('admin.turn.index',compact('turn'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.turn.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $agents=Agent::where('status',1);
            $turn= new Turn;
            $turn->description= preg_replace('[\s+]','', $request->description);
            $turn->save();
            return redirect('admin/shifts')->with('message','Registro creado correctamente.');
        } catch (Exception $e) {
            return redirect('admin/shifts')->with("error", "No se pudo realizar la acción.");
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

      try {
        $agent=Agent::where('id_turn',$id)->get();
        return view('admin.turn.show',compact('agent'));
      } catch (\Exception $e) {
          abort(404);
      }



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verifycreate(Request $request)
    {
        $number = Turn::where('description', $request->dataArray['description'])->count();
        $response = ($number == 0) ? "ok" : "error";
        return $response;
    }

    public function showState($id,$value)
    {
        $number=Turn::where('id',$id)->count();
        if($number>0)
          return view('admin.turn.showState',compact('id','value'));
        else
          abort(404);
    }

    public function changeState($id,$value)
    {
        try {
            $agents=Agent::where('id_turn',$id)->get();
            foreach ($agents as $key => $value) {
                  foreach ($value->users as $key => $user) {
                      $user->status=0;
                      $user->save();
                  }
            }
            $turn=Turn::findOrFail($id);
            $turn->state=$value;
            $turn->save();
            return redirect('admin/shifts')->with('message','Operacion Generada correctamente.');
        } catch (\Exception $e) {
            return redirect('admin/shifts')->with("error", "No se pudo realizar la acción.".$e->getMessage());
        }

    }
}
