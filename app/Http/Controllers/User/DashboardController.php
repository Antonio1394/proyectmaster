<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use JavaScript;



class DashboardController extends Controller
{
    public function index(Request $request)
    {
        // $alerts = Alert::where('state', 1)->orderBy('id', 'desc')->get();
        // $alerts52 = Alert52::where('state', 1)->orderBy('id', 'desc')->get();
        // $alertscar = AlertCar::where('state', 1)->orderBy('id', 'desc')->get();
        // $alertsstole = AlertStole::where('state', 1)->orderBy('id', 'desc')->get();
        // $alertsDeath = AlertDeath::where('state', 1)->orderBy('id', 'desc')->get();

        // $alertsArray = [];
        // $count = 0;

        // foreach ($alerts52 as $key => $value) {
        //   $alertsArray[$count] = [
        //     'alert_type' => 'Alerta 52',
        //     'code_type' => 1,
        //     'description' => $value->description,
        //     'date' => date("d/m/Y", strtotime($value->date)) . ' ' . $value->hour,
        //     'create' => $value->created_at
        //   ];

        //   $count++;
        // }

        // foreach ($alertscar as $key => $value) {
        //   $alertsArray[$count] = [
        //     'alert_type' => 'Alerta Robo Vehiculo',
        //     'code_type' => 2,
        //     'type' => $value->type,
        //     'brand' => $value->brand,
        //     'color' => $value->color,
        //     'plate' => $value->plate,
        //     'person' => $value->person,
        //     'address' => $value->address,
        //     'date' => date("d/m/Y", strtotime($value->date)) . ' ' . $value->hour,
        //     'create' => $value->created_at
        //   ];

        //   $count++;
        // }

        // foreach ($alertsstole as $key => $value) {
        //   $alertsArray[$count] = [
        //     'alert_type' => 'Alerta Robo General',
        //     'code_type' => 3,
        //     'robbery' => $value->robbery,
        //     'individual' => $value->individual,
        //     'object' => $value->object,
        //     'person' => $value->person,
        //     'address' => $value->address,
        //     'date' => date("d/m/Y", strtotime($value->date)) . ' ' . $value->hour,
        //     'create' => $value->created_at
        //   ];

        //   $count++;
        // }

        // foreach ($alerts as $key => $value) {
        //   $alertsArray[$count] = [
        //     'alert_type' => 'Alerta General',
        //     'code_type' => 4,
        //     'description' => $value->description,
        //     'address' => $value->address,
        //     'date' => date("d/m/Y", strtotime($value->date)) . ' ' . $value->hour,
        //     'create' => $value->created_at
        //   ];

        //   $count++;
        // }

        // foreach ($alertsDeath as $key => $value) {
        //   $alertsArray[$count] = [
        //     'alert_type' => 'Alerta Heridos/Fallecidos',
        //     'code_type' => 5,
        //     'quantity' => $value->quantity,
        //     'personType' => $value->personType,
        //     'alertType' => $value->alertType,
        //     'address' => $value->address,
        //     'cause' => $value->cause,
        //     'date' => date("d/m/Y", strtotime($value->date)) . ' ' . $value->hour,
        //     'create' => $value->created_at
        //   ];

        //   $count++;
        // }

        // usort($alertsArray, function($a, $b) {
        //   $ad = new \DateTime($a['create']);
        //   $bd = new \DateTime($b['create']);

        //   if ($ad == $bd) {
        //     return 0;
        //   }

        //   return $ad > $bd ? -1 : 1;
        // });

        // JavaScript::put([
        //   'alerts' => $alertsArray,
        //   'user_agent' => $request->user()->agent
        // ]);

        return view('general.index');
    }

    public function indexUbication(Request $request)
    {
      JavaScript::put([
        'user_agent' => $request->user()->agent
      ]);
      
      return view('general.ubication.index');
    }

    public function saveUbication(Request $request)
    {
        $user=User::findOrFail($request->dataArray['id']);
        if(empty($user)){
          return 'error';
        }else{
          $user->latitude=$request->dataArray['lt'];
          $user->longitude=$request->dataArray['ln'];
          $user->save();
          return 'ok';
        }
    }
}
