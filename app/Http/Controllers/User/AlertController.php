<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Alert;
use App\Models\AlertCar;
use App\Models\AlertStole;
use App\Models\Alert52;
use App\Models\AlertDeath;


use App\Events\AlertEvent;

use JavaScript;

class AlertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        JavaScript::put([
          'user_agent' => $request->user()->agent
        ]);

        $alerts=Alert::orderBy('id','desc')->
                       where('id_user',Auth()->user()->id)->get();
        return view('general.alert.index',compact('alerts'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('general.alert.create');
    }

    public function createCar()
    {
      return view('general.alert.alertCar.create');
    }

    public function createStole()
    {
      return view('general.alert.alertStole.create');
    }

  public function create52()
  {
    return view('general.alert.alert52.create');
  }

  public function createDeath()
  {
    return view('general.alert.alertDeath.create');
  }

    public function saveAlert(Request $request)
    {

        try {
            $alert = new Alert;
            $time=getdate();
            $alert->description=$request->dataArray['description'];
            $alert->latitude=$request->dataArray['latitude'];
            $alert->longitude=$request->dataArray['longitude'];
            $alert->address=$request->dataArray['address'];
            $alert->date=$time['year'].'-'.$time['mon'].'-'.$time['mday'];
            $alert->hour=$time['hours'].':'.$time['minutes'].'-'.$time['seconds'];
            $alert->id_user=Auth()->user()->id;
            $alert->save();

            event(new AlertEvent([
              'alert_type' => 'Alerta General',
              'code_type' => 4,
              'description' => $alert->description,
              'address' => $alert->address,
              'date' => date("d/m/Y", strtotime($alert->date)) . ' ' . $alert->hour,
              'create' => $alert->created_at
            ]));

            return "ok";
        } catch (Exception $e) {
            return "error";
        }

    }

    public function saveAlertCar(Request $request)
    {
        try {
          $alert=new AlertCar;
          $time=getdate();
          $alert->latitude=$request->dataArray['latitude'];
          $alert->longitude=$request->dataArray['longitude'];
          $alert->type=$request->dataArray['type'];
          $alert->mode=$request->dataArray['mode'];
          $alert->brand=$request->dataArray['brand'];
          $alert->address=$request->dataArray['address'];
          $alert->color=$request->dataArray['color'];
          $alert->plate=$request->dataArray['plate'];
          $alert->person=$request->dataArray['person'];
          $alert->date=$time['year'].'-'.$time['mon'].'-'.$time['mday'];
          $alert->hour=$time['hours'].':'.$time['minutes'].'-'.$time['seconds'];
          $alert->state=1;
          $alert->id_user=Auth()->user()->id;
          $alert->save();

          event(new AlertEvent([
            'alert_type' => 'Alerta Robo Vehiculo',
            'code_type' => 2,
            'type' => $alert->type,
            'brand' => $alert->brand,
            'color' => $alert->color,
            'plate' => $alert->plate,
            'person' => $alert->person,
            'address' => $alert->address,
            'date' => date("d/m/Y", strtotime($alert->date)) . ' ' . $alert->hour,
            'create' => $alert->created_at
          ]));

          return "ok";
      } catch (\Exception $e) {
          return "error".$e->getMessage();
        }

    }

    public function saveAlertStole(Request $request)
    {
        try {
            $alert=new AlertStole;
            $time=getdate();
            $alert->latitude=$request->dataArray['latitude'];
            $alert->longitude=$request->dataArray['longitude'];
            $alert->robbery=$request->dataArray['roberry'];
            $alert->individual=$request->dataArray['individual'];
            $alert->address=$request->dataArray['address'];
            $alert->object=$request->dataArray['object'];
            $alert->person=$request->dataArray['person'];
            $alert->date=$time['year'].'-'.$time['mon'].'-'.$time['mday'];
            $alert->hour=$time['hours'].':'.$time['minutes'].'-'.$time['seconds'];
            $alert->state=1;
            $alert->id_user=Auth()->user()->id;
            $alert->save();

            event(new AlertEvent([
                'alert_type' => 'Alerta Robo General',
                'code_type' => 3,
                'robbery' => $alert->robbery,
                'individual' => $alert->individual,
                'object' => $alert->object,
                'person' => $alert->person,
                'address' => $alert->address,
                'date' => date("d/m/Y", strtotime($alert->date)) . ' ' . $alert->hour,
                'create' => $alert->created_at
              ]));

            return "ok";
        } catch (\Exception $e) {
            return "error".$e->getMessage();
        }

    }

    public function saveAlert52(Request $request)
    {

      try {
        $alert=new Alert52;
        $time=getdate();
        $alert->latitude=$request->dataArray['latitude'];
        $alert->longitude=$request->dataArray['longitude'];
        $alert->description=$request->dataArray['description'];
        $alert->date=$time['year'].'-'.$time['mon'].'-'.$time['mday'];
        $alert->hour=$time['hours'].':'.$time['minutes'].'-'.$time['seconds'];

        $alert->state=1;
        $alert->id_user=Auth()->user()->id;
        $alert->save();

        event(new AlertEvent([
            'alert_type' => 'Alerta 52',
            'code_type' => 1,
            'description' => $alert->description,
            'date' => date("d/m/Y", strtotime($alert->date)) . ' ' . $alert->hour,
            'create' => $alert->created_at
          ]));

        return "ok";
      } catch (Exception $e) {

      }


    }

    public function saveAlertDeath(Request $request)
    {
        try {
            $alert=new AlertDeath;
            $time=getdate();
            $alert->latitude=$request->dataArray['latitude'];
            $alert->longitude=$request->dataArray['longitude'];
            $alert->quantity=$request->dataArray['quantity'];
            $alert->personType=$request->dataArray['personType'];
            $alert->alertType=$request->dataArray['alertType'];
            $alert->address=$request->dataArray['address'];
            $alert->cause=$request->dataArray['cause'];
            $alert->date=$time['year'].'-'.$time['mon'].'-'.$time['mday'];
            $alert->hour=$time['hours'].':'.$time['minutes'].'-'.$time['seconds'];
            $alert->state=1;
            $alert->id_user=Auth()->user()->id;
            $alert->save();

            event(new AlertEvent([
              'alert_type' => 'Alerta Heridos/Fallecidos',
              'code_type' => 5,
              'quantity' => $alert->quantity,
              'personType' => $alert->personType,
              'alertType' => $alert->alertType,
              'address' => $alert->address,
              'cause' => $alert->cause,
              'date' => date("d/m/Y", strtotime($alert->date)) . ' ' . $alert->hour,
              'create' => $alert->created_at
            ]));

            return "ok";
        } catch (\Exception $e) {
            return "error".$e->getMessage();
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
