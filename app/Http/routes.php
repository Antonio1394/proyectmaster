<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::post('auth/verify', 'Auth\AuthController@verifyLogin');


Route::group(['prefix' => 'admin', 'namespace' => '\Admin', 'middleware' => ['auth', 'administrator']], function() {

    Route::get('/', 'DashboardController@index');

    // Rutas del AgentsController
    Route::get('/agents/showSuspending','AgentsController@showSuspend');
    Route::get('/agents/showBreak','AgentsController@showBreak');
    Route::get('agents/showdelete/{id}', 'AgentsController@showdelete');
    Route::post('agents/verifycreate', 'AgentsController@verifycreate');
    Route::post('agents/verifyedit', 'AgentsController@verifyedit');
    Route::post('agents/verifyedit', 'AgentsController@verifyedit');
    Route::get('agents/showChange/{id}', 'AgentsController@showChange');
    Route::put('agents/activate/{id}','AgentsController@build');
    Route::get('agents/showState/{id}/{value}', 'AgentsController@showState');
    Route::put('agents/changeState/{id}/{value}','AgentsController@changeState');
    Route::post('agents/verifyNip','AgentsController@verifyNip');
    Route::post('agents/verifyeditNip','AgentsController@verifyeditNip');

    Route::resource('agents', 'AgentsController');
    //Rutas de Usuarios
    Route::get('users/save/{id}','UsersController@create');
    Route::post('users/verifycreate', 'UsersController@verifycreate');
    Route::post('users/verifyedit', 'UsersController@verifyedit');
    Route::get('users/showUp/{id}','UsersController@showUp');
    Route::get('users/showDown/{id}','UsersController@showDown');
    Route::put('users/activate/{id}',['as'=>'admin/users/activate','uses'=>'UsersController@build']);
    Route::resource('users','UsersController');
    ////Rutas de turnos
    Route::post('shifts/verifycreate', 'ShiftsController@verifycreate');
    Route::get('shifts/showState/{id}/{value}', 'ShiftsController@showState');
    Route::put('shifts/changeState/{id}/{value}','ShiftsController@changeState');
    Route::resource('shifts','ShiftsController');


});

Route::group(['prefix' => 'user', 'namespace' => '\User', 'middleware' => ['auth', 'agent']], function() {
    Route::get('/', 'DashboardController@index');
});

Route::get('/errors', function () {
    return view('errors.page-500');
});
