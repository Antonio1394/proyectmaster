<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AgentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'    => 'required',
            'last_name'     => 'required',
            'dpi'           => 'required|unique:agents,dpi',
            'id_turn'       => 'required',
            'nip'           => 'required|unique:agents,nip',
            'age'           => 'required|integer',
            'phone'         => 'required|digits_between:8,8',
            'address'       => 'required',
            'image'         => 'image|mimes:jpeg,png,jpg,JPGE,PNG,JPG|max:250000',
            'rank'          => 'required|in:Comisario,Inspector,Oficial',
        ];
    }
}
